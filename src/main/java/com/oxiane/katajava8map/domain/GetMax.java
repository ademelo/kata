package com.oxiane.katajava8map.domain;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class GetMax {

    public static Optional<Integer> getMaxValueFromCollection(List<Integer> collection) {
        return collection.stream()
                .max(Comparator.comparing(Integer::valueOf));
    }

}
