package com.oxiane.katajava8map.domain;

import java.util.List;
import java.util.stream.Collectors;

public class Filter {

    public static List <String> filterOnLength(List<String> collection, int len) {
        return collection.stream()
                .filter(elmt -> elmt.length() >= len)
                .collect(Collectors.toList());
    }

}
