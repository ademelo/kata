package com.oxiane.katajava8map.domain;

import org.junit.Test;

import java.util.List;

import static com.oxiane.katajava8map.domain.Converter.elementsToUpperCase;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class ConvertElementsToUpperCaseTest {

    @Test
    public void should_convert_elements_to_upper_case() {
        List<String> collection = asList("toto", "titi", "tutu");
        List<String> expected = asList("TOTO", "TITI", "TUTU");
        assertThat(elementsToUpperCase(collection), is(expected));
    }

    @Test
    public void should_return_elements_already_in_upper_case() {
        List<String> collection = asList("TUTU", "TOTO");
        List<String> expected = asList("TUTU", "TOTO");
        assertThat(elementsToUpperCase(collection), is(expected));
    }

}
