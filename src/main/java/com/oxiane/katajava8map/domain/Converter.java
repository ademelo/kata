package com.oxiane.katajava8map.domain;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class Converter {

    public static List<String> elementsToUpperCase(List<String> list) {
        return list.stream()
                .map(elmt -> elmt.toUpperCase())
                .collect(Collectors.toList());
    }

}
