package com.oxiane.katajava8map.domain;

import lombok.Getter;

@Getter
public class Person {

    private final String name;
    private final Integer age;

    Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }
}
