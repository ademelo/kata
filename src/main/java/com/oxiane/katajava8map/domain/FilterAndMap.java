package com.oxiane.katajava8map.domain;

import java.util.List;
import java.util.stream.Collectors;

public class FilterAndMap {
    public static List<String> getKidsNames(List<Person> persons) {
        return persons.stream()
                .filter(p -> p.getAge() < 18)
                .map(Person::getName)
                .collect(Collectors.toList());
    }
}
