package com.oxiane.katajava8map.domain;

import org.junit.Test;

import java.util.List;

import static com.oxiane.katajava8map.domain.Flattener.flattenCollection;
import static java.util.Arrays.asList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class FlatCollectionTest {

    @Test
    public void should_flatten_collection1() {
        List<List<String>> collection = asList(asList("1", "2", "3"), asList("4", "5"));
        List<String> expected = asList("1", "2", "3", "4", "5");
        assertThat(flattenCollection(collection), is(expected));
    }

    @Test
    public void should_flatten_collection2() {
        List<List<String>> collection = asList(asList("1", "2", "3"), asList("4", "5","6"));
        List<String> expected = asList("1", "2", "3", "4", "5","6");
        assertThat(flattenCollection(collection), is(expected));
    }

}
