package com.oxiane.katajava8map;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Stack;
import java.util.stream.IntStream;

import static com.oxiane.katajava8map.domain.KataSG.printFooBarQixOrNumber;
import static com.oxiane.katajava8map.domain.KataSGChristophe.printNumberOrString;

public class Codility {

    public static void main(String[] args) {
        /*int[] tab = {9,3,9,3,9,7,9};
        findUnpairElement(tab);*/
        /*String S1 = "{[()()]}";
        String S2 = "([)()]";
        String S3 = "{[()(]}";
        brakets(S1);
        brakets(S2);
        brakets(S3);*/
        /*int[] A = {2,1,1,3,2,1,1,3};
        int[] B = {7,5,2,7,2,7,4,7};
        System.out.println(findVacations(A));
        System.out.println(findVacations(B));*/

        /*System.out.println(findBinaryGap(9));
        System.out.println(findBinaryGap(529));
        System.out.println(findBinaryGap(15));
        System.out.println(findBinaryGap(32));
        System.out.println(findBinaryGap(2000000001));*/

        //printFooBarQixOrNumberKata();

        printNumberKataChristophe();

        /*System.out.print("--------- IntStream TEST ---------");
        IntStream.of(345)
                .filter(n -> n < 4)
                .forEach(x -> System.out.println(x));*/

    }

    public static void printNumberKataChristophe() {
        for(int i=1; i<=100; i++) {
            System.out.println(printNumberOrString(i));
        }
    }

    public static void printFooBarQixOrNumberKata() {
        for(int i=1; i<=100; i++) {
            System.out.println(printFooBarQixOrNumber(i));
        }
    }

    public static int findBinaryGap(int N) {
        String binaryInteger = Integer.toBinaryString(N);
        int binGap=0;
        int maxBinGap=0;
        if(binaryInteger.charAt(binaryInteger.length()-1) == '0') return 0;
        for(int i=0, len=binaryInteger.length(); i<len; i++) {
            if(binaryInteger.charAt(i) == '0') {
                binGap++;
            } else {
                if(binGap > maxBinGap) maxBinGap = binGap;
                binGap = 0;
            }
        }
        return maxBinGap;
    }

    public static int findVacations(int[] A) {
        int[] noDuplicates = IntStream.of(A).distinct().toArray();
        //Stack<Integer> pile = new Stack<>();
        HashSet<Integer> vacationsSet = new HashSet<>();
        int count = 0;
        int countFinal = A.length;
        int tmp = 0;
        for (int j=0, l=A.length - noDuplicates.length+1; j<l; j++) {
            for (int i = j, len = A.length; i < len; i++) {
                /*if (pile.size() == noDuplicates.length) break;
                if (!pile.contains(A[i])) {
                    pile.push(A[i]);
                }*/
                if (!vacationsSet.contains(A[i])) {
                    vacationsSet.add(A[i]);
                }
                if (vacationsSet.size() == noDuplicates.length) break;
                count++;
            }
            if(count == noDuplicates.length) return count;
            if (count <= countFinal) countFinal = count;
            count = 0;
            vacationsSet.clear();
        }
        return countFinal;
    }

    public static boolean isValid(char deb, char fin) {
        if (deb == '(' && fin == ')') return true;
        if (deb == '{' && fin == '}') return true;
        if (deb == '[' && fin == ']') return true;
        return false;
    }

    public static int brakets(String S) {
        if (S.length() % 2 == 1) return 0;
        Stack<Character> pile = new Stack<>();
        for (int i=0, len = S.length(); i<len; i++) {
            char car = S.charAt(i);
            if ((car == '[') || (car == '{') || (car == '(')) {
                pile.push(car);
            } else if (pile.empty()) {
                return 0;
            } else if (! isValid(pile.pop(), car)) {
                return 0;
            }
        }
        if (pile.empty()) return 1;
        return 0;
    }
    public static void findUnpairElement(int[] A) {

    }

    public static void cyclicRotation() {
        int[] tab = {3, 8, 9, 7, 6};
        rotation(tab, 3);
        int[] tab2 = {0,0,0};
        rotation(tab2, 1);
        int[] tab3 = {1, 2, 3, 4};
        rotation(tab3, 4);
    }

    public static void rotation(int[] A, int n) {
        for(int i=0; i<n; i++) {
            int tmp = A[A.length-1];
            for(int j=A.length-1; j>0; j--) {
                A[j] = A[j-1];
            }
            A[0] = tmp;
        }
        Arrays.stream(A).forEach(e -> System.out.println(e));
    }

}
