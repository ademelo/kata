package com.oxiane.katajava8map.domain;

import org.junit.Test;

import java.util.List;

import static com.oxiane.katajava8map.domain.FilterAndMap.getKidsNames;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class FilterAndMapTest {

    @Test
    public void get_kid_name_should_return_all_kids_names() {
        Person sara = new Person("Sara", 17);
        Person paul = new Person("Paul", 48);
        Person damian = new Person("Damian", 8);
        List<Person> persons = asList(sara, paul, damian);
        assertThat(getKidsNames(persons))
                .contains("Sara", "Damian")
                .doesNotContain("Paul");
    }

}
