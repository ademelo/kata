package com.oxiane.katajava8map.domain;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.time.Period;

import static java.time.Period.*;

@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Client {

    private Long id;
    private String name;
    private String surname;
    private LocalDate birthday;

    public Client(Long id, String name, String surname, LocalDate birthday) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

    public Integer getAge() {
        LocalDate today = LocalDate.now();
        Period period = between(birthday, today);
        return period.getYears();
    }

}
