package com.oxiane.katajava8map.domain;

public class KataSG {

    public static String printFooBarQixOrNumber(int number) {
        if(number % 5 == 0) {
            return "Bar";
        }

        if(number % 7 == 0) {
            return "Qix";
        }
        if(number % 3 == 0) {
            return "Foo";
        }
        return Integer.toString(number);
    }
}
