package com.oxiane.katajava8map.domain;

import org.junit.Test;

import java.util.List;
import java.util.NoSuchElementException;

import static com.oxiane.katajava8map.domain.GetMax.getMaxValueFromCollection;
import static java.util.Arrays.asList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class GetMaxTest {

    @Test
    public void should_return_maximum_value_from_collection1() {
        List<Integer> collection = asList(10, 20, 89, 4);
        assertThat(getMaxValueFromCollection(collection).get(), is(89));
    }

    @Test
    public void should_return_maximum_value_from_collection2() {
        List<Integer> collection = asList(10, 20, 8, 4);
        assertThat(getMaxValueFromCollection(collection).get(), is(20));
    }

    @Test(expected = NoSuchElementException.class)
    public void should_return_maximum_value_from_collection3() {
        List<Integer> collection = asList();
        assertThat(getMaxValueFromCollection(collection).get(), is(20));
    }

}
