package com.oxiane.katajava8map.domain;

import org.junit.Test;

import static com.oxiane.katajava8map.domain.KataSG.printFooBarQixOrNumber;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class KataSGTest {

    @Test
    public void should_print_foo_when_divisible_by_3() {
        assertThat(printFooBarQixOrNumber(3), is("Foo"));
        assertThat(printFooBarQixOrNumber(9), is("Foo"));
        assertThat(printFooBarQixOrNumber(18), is("Foo"));
    }

    @Test
    public void should_print_bar_when_divisible_by_5() {
        assertThat(printFooBarQixOrNumber(5), is("Bar"));
        assertThat(printFooBarQixOrNumber(10), is("Bar"));
        assertThat(printFooBarQixOrNumber(25), is("Bar"));
    }

    @Test
    public void should_print_qix_when_divisible_by_7() {
        assertThat(printFooBarQixOrNumber(7), is("Qix"));
        assertThat(printFooBarQixOrNumber(14), is("Qix"));
        assertThat(printFooBarQixOrNumber(21), is("Qix"));
    }

    @Test
    public void else_should_print_number_value() {
        assertThat(printFooBarQixOrNumber(2), is("2"));
    }
}
