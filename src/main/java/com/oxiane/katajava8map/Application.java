package com.oxiane.katajava8map;

import com.oxiane.katajava8map.domain.Client;
import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.ls.LSInput;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class Application {

    public static void main(String[] args) {
        List<Client> clients = new ArrayList<>();
        clients.add(new Client(1L, "user1", "user1", LocalDate.of(1960, Month.JANUARY, 1)));
        clients.add(new Client(2L, "user2", "user2", LocalDate.of(1980, Month.DECEMBER, 10)));
        clients.add(new Client(3L, "user3", "user3", LocalDate.of(1990, Month.JULY, 31)));
        clients.add(new Client(4L, "user4", "user4", LocalDate.of(1962, Month.JANUARY, 21)));
        clients.add(new Client(5L, "user5", "user5", LocalDate.of(2001, Month.FEBRUARY, 8)));

        List<Client> underAgeClient = clients.stream()
                .filter(c -> c.getAge() > 30)
                .collect(Collectors.toList());

        underAgeClient.forEach(c -> System.out.println(c.toString()));

        List<Client> bornInJanuay = clients.stream()
                .filter(c -> c.getBirthday().getMonth() == Month.JANUARY)
                .collect(Collectors.toList());

        LOGGER.info("Born in January:", "");
        bornInJanuay.forEach(c -> System.out.println(c.toString()));

        Double averageAge = clients.stream()
                .mapToInt(Client::getAge)
                .average()
                .getAsDouble();

        LOGGER.info("Clients average age is {} yearsold", averageAge);

        LOGGER.info("Flatmap vs Map exemple {}", "");

        List<List<Client>> listOfListOfClient = new ArrayList<>();
        listOfListOfClient.add(clients);

        System.out.println(listOfListOfClient.stream()
                .map(Collection::stream)
                .collect(Collectors.toList()));

        System.out.println(listOfListOfClient.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList()));

    }

}
