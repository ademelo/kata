package com.oxiane.katajava8map.domain;

import org.apache.logging.log4j.util.StringMap;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class KataChiffreRomain {

    private static final LinkedHashMap<Integer, String> VALUES_SYMBOLS = new LinkedHashMap<>();
    static {
        VALUES_SYMBOLS.put(20, "XX");
        VALUES_SYMBOLS.put(19, "XIX");
        VALUES_SYMBOLS.put(15, "XV");
        VALUES_SYMBOLS.put(14, "XIV");
        VALUES_SYMBOLS.put(10, "X");
        VALUES_SYMBOLS.put(9, "IX");
        VALUES_SYMBOLS.put(5, "V");
        VALUES_SYMBOLS.put(4, "IV");
    }

    public static String arabicToRomanNotation(int number) {
        StringBuilder convertingResult = new StringBuilder();
        int remaining = number;

        /*remaining = appendRomanNumeral(remaining, 20, "XX", convertingResult);
        remaining = appendRomanNumeral(remaining, 19, "XIX", convertingResult);
        remaining = appendRomanNumeral(remaining, 15, "XV", convertingResult);
        remaining = appendRomanNumeral(remaining, 14, "XIV", convertingResult);
        remaining = appendRomanNumeral(remaining, 10, "X", convertingResult);
        remaining = appendRomanNumeral(remaining, 9, "IX", convertingResult);
        remaining = appendRomanNumeral(remaining, 5, "V", convertingResult);
        remaining = appendRomanNumeral(remaining, 4, "IV", convertingResult);*/

        for(Map.Entry<Integer, String> value_symbol : VALUES_SYMBOLS.entrySet()) {
            remaining = appendRomanNumeral(remaining, value_symbol.getKey(), value_symbol.getValue(), convertingResult);
        }

        for (int i = 0; i < remaining; i++) {
            convertingResult.append("I");
        }

        return convertingResult.toString();
    }

    private static int appendRomanNumeral(int number, int value, String romanDigits, StringBuilder convertingResult) {
        int result = number;
        if(result >= value) {
            convertingResult.append(romanDigits);
            result = result - value;
        }
        return result;
    }
}
