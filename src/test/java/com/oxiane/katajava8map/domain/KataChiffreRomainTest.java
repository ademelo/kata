package com.oxiane.katajava8map.domain;

import org.junit.Test;

import static com.oxiane.katajava8map.domain.KataChiffreRomain.arabicToRomanNotation;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class KataChiffreRomainTest {

    @Test
    public void should_return_one_two_three() {
        assertThat(arabicToRomanNotation(1), is("I"));
        assertThat(arabicToRomanNotation(2), is("II"));
        assertThat(arabicToRomanNotation(3), is("III"));
    }

    @Test
    public void should_return_four() {
        assertThat(arabicToRomanNotation(4), is("IV"));
    }

    @Test
    public void should_return_five() {
        assertThat(arabicToRomanNotation(5), is("V"));
    }

    @Test
    public void should_return_six() {
        assertThat(arabicToRomanNotation(6), is("VI"));
    }

    @Test
    public void should_return_seven() {
        assertThat(arabicToRomanNotation(7), is("VII"));
    }

    @Test
    public void should_return_eight() {
        assertThat(arabicToRomanNotation(8), is("VIII"));
    }

    @Test
    public void should_return_nine() {
        assertThat(arabicToRomanNotation(9), is("IX"));
    }

    @Test
    public void should_return_ten() {
        assertThat(arabicToRomanNotation(10), is("X"));
    }

    @Test
    public void should_return_eleven() {
        assertThat(arabicToRomanNotation(11), is("XI"));
    }

    @Test
    public void should_return_twelve() {
        assertThat(arabicToRomanNotation(12), is("XII"));
    }

    @Test
    public void should_return_thirteen() {
        assertThat(arabicToRomanNotation(13), is("XIII"));
    }

    @Test
    public void should_return_fourteen() {
        assertThat(arabicToRomanNotation(14), is("XIV"));
    }

    @Test
    public void should_return_fifteen() {
        assertThat(arabicToRomanNotation(15), is("XV"));
    }

    @Test
    public void should_return_sixteen() {
        assertThat(arabicToRomanNotation(16), is("XVI"));
    }

    @Test
    public void should_return_seventeen() {
        assertThat(arabicToRomanNotation(17), is("XVII"));
    }

    @Test
    public void should_return_eighteen() {
        assertThat(arabicToRomanNotation(18), is("XVIII"));
    }

    @Test
    public void should_return_19_20_21_22_23() {
        assertThat(arabicToRomanNotation(19), is("XIX"));
        assertThat(arabicToRomanNotation(20), is("XX"));
        assertThat(arabicToRomanNotation(21), is("XXI"));
        assertThat(arabicToRomanNotation(22), is("XXII"));
        assertThat(arabicToRomanNotation(23), is("XXIII"));
    }
}
