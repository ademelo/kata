package com.oxiane.katajava8map.domain;

import org.junit.Test;

import static com.oxiane.katajava8map.domain.KataSGChristophe.printNumberOrString;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class KataSGChristopheTest {

    @Test
    public void should_return_foo_when_divisible_by_3() {
        assertThat(printNumberOrString(9), is("Foo"));
    }

    @Test
    public void should_return_foo_when_contain_3() {
        assertThat(printNumberOrString(13), is("Foo"));
    }

    @Test
    public void should_return_foofoo_when_divisible_by_3_and_contain_3() {
        assertThat(printNumberOrString(3), is("FooFoo"));
    }

    @Test
    public void should_return_bar_when_divisible_by_5() {
        assertThat(printNumberOrString(20), is("Bar"));
    }

    @Test
    public void should_return_barbar_when_divisible_by_5_and_contain_5() {
        assertThat(printNumberOrString(5), is("BarBar"));
    }

    @Test
    public void should_return_qix_when_divisible_by_7() {
        assertThat(printNumberOrString(14), is("Qix"));
    }

    @Test
    public void should_return_qixqix_when_divisible_by_7_and_contain_7() {
        assertThat(printNumberOrString(7), is("QixQix"));
    }

    @Test
    public void should_return_foobar_when_divisible_by_3_and_contain_5() {
        assertThat(printNumberOrString(51), is("FooBar"));
    }

    @Test
    public void should_return_barfoo_when_contains_first_5_then_3() {
        assertThat(printNumberOrString(53), is("BarFoo"));
    }

    @Test
    public void should_return_foobarbar_when_divisible_by_3_and_5_and_contain_5() {
        assertThat(printNumberOrString(15), is("FooBarBar"));
    }


    @Test
    public void should_return_foofoofoo_when_divisible_by_3_and_contain_3_two_times() {
        assertThat(printNumberOrString(33), is("FooFooFoo"));
    }

    @Test
    public void else_should_return_number() {
        assertThat(printNumberOrString(1), is("1"));
    }
}
