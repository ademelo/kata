package com.oxiane.katajava8map.domain;

import org.junit.Test;

import java.util.List;

import static com.oxiane.katajava8map.domain.Filter.filterOnLength;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class FilterCollectionTest {

    @Test
    public void should_filter_collection1() {
        List<String> collection = asList("My", "name", "is", "Tutu", "WhyNot?");
        List<String> expected = asList("name", "Tutu", "WhyNot?");
        assertThat(filterOnLength(collection, 4), is(expected));
    }

    @Test
    public void should_filter_collection2() {
        List<String> collection = asList("My", "name", "is", "Tutu", "Wut");
        List<String> expected = asList("name", "Tutu");
        assertThat(filterOnLength(collection, 4), is(expected));
    }

    @Test
    public void should_filter_collection_for_length_parameter() {
        List<String> collection = asList("My", "name", "is", "Tutu", "Wutututu?");
        List<String> expected = asList("Wutututu?");
        assertThat(filterOnLength(collection, 5), is(expected));
    }

}
