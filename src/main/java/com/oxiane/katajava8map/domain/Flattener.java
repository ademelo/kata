package com.oxiane.katajava8map.domain;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Flattener {

    public static List<String> flattenCollection(List<List<String>> collection) {
        return collection.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

}
