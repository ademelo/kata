package com.oxiane.katajava8map.domain;

import java.util.stream.IntStream;

public class KataSGChristophe {

    public static String printNumberOrString(int number) {
        String resultToPrint = "";
        if(number % 3 == 0) {
            resultToPrint = resultToPrint + "Foo";
        }
        if(number % 5 == 0) {
            resultToPrint = resultToPrint + "Bar";
        }
        if(number % 7 == 0) {
            resultToPrint = resultToPrint + "Qix";
        }

        String numberToIterate = String.valueOf(number);
        for(int i=0, len=numberToIterate.length(); i<len; i++) {
            String FooBarQix = "";
            switch (numberToIterate.charAt(i)) {
                case '3':
                    FooBarQix = "Foo";
                    break;
                case '5':
                    FooBarQix = "Bar";
                    break;
                case '7':
                    FooBarQix = "Qix";
                    break;
            }
            resultToPrint = resultToPrint + FooBarQix;
        }

        if(resultToPrint.isEmpty()) return String.valueOf(number);

        return resultToPrint;
    }

}
